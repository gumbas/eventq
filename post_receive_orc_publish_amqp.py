#!/usr/bin/python
# Copyright 2013 CollabNet, Inc.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# post-receive script for a git repository to notify an amqp server of 
# commit activity.

__revision__ = ""

import json
import os
import sys

from orchestrate.config import HookConfig
from orchestrate.amqp   import AmqpServerConnection
from orchestrate.utils  import create_logger
from orchestrate.utils  import fix_date
from orchestrate.git    import GitTool

LOG = None

def main():
    global LOG
    cfg_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), 
                            'post_receive_orc_publish_amqp.conf')
    cfg = HookConfig(cfg_file=cfg_file)
    LOG = create_logger(cfg=cfg)

    cfg.validate()

    amqp_srvr = AmqpServerConnection(cfg=cfg, logger=LOG)
    amqp_srvr.connect()

    # post-receive expects to receive data on stdin, one triplet per line of 
    # (old_ref, new_ref, ref_branch_name).  
    # See: http://git-scm.com/book/en/Customizing-Git-Git-Hooks
    for line in sys.stdin:
        try:
            old, new, refname = line.split()
        except Exception as e:
            LOG.critical("Incorrect number of args on stdin (3 expected): %s" % e)
            continue
        branch = GitTool().get_branch_name(refname)
        for ref in GitTool().get_commits(old, new):
            commit = GitCommit(ref, refname, branch, cfg)
            LOG.debug("as_struct: %s" % commit.as_struct())
            LOG.debug("as_json: %s" % commit.as_json(pretty=True))
            amqp_srvr.send(commit.as_json())
    amqp_srvr.close()
            
class GitCommit(object):
    """
    Object representing a single git commit.
    """
    
    def __init__(self, ref, refname, branch, cfg, tool=GitTool()):
        self.tool = tool
    
        self.cfg = cfg
        self.ref = ref
        self.refname = refname # TODO: drop if not needed
        self.branch = branch

        self.user = None
        self.message = ''
        self.changed = []
        self.timestamp = None

        data = self.tool.get_commit_data(self.ref, **self.cfg.git)

        self.user = data.user
        self.message = data.message
        self.changed = data.changed
        
        self.timestamp = data.timestamp        
        self.source_association_key =\
        self.cfg.orchestrate['source_association_key']

    def _fix_changes(self):
        """
        Convert the list of change lists into a list of hashes, suitable for 
        reporting to TeamForge Orchestrate.
        """
        # Type codes which can be reported back by `git log`, applicable to the
        # context of viewing a single commit:
        action_map = {
            'm': 'modified',
            'a': 'added',
            'd': 'deleted',
            't': 'type_changed', 
            'c': 'copied',       # with % accuracy, src, dest
            'r': 'renamed',      # same
        }
        changed = []
        # Changed data is an array of either two or three elements (depending if
        # the operation was one of add/modify/delete/typechange, or copy/rename), 
        # consisting roughly of typecode, (optional from_path), path.  In the cases
        # where a copy or rename was detected, typecode will be a four-character 
        # sequence, where the first character is the actual typecode, and the last
        # three are the percentage of 'confidence' in the identification as a copy
        # or rename.
        #
        # e.g., ['A', '/some/file/path'] or ['R090', '/from/path', '/to/path']
        for stat in self.changed:
            change_hash = {'action': action_map[stat[0][0].lower()], 'path': stat[-1]}
            if len(stat) == 3: # if copy or rename detected
                change_hash['from'] = stat[1]
            changed.append(change_hash)
        return changed

    def as_struct(self):
        """
        Return the activity as a data structure, suitable for output as json.
        """
        return {
            "api_version": "1",
            "source_association_key": self.source_association_key,
            "commit_data" : {
                "revision_id": self.ref,
                "branch_name": self.branch,
                "type":        "post_commit",
                "event_time":  fix_date(self.timestamp),
                "created_by":  self.user,
                "message":     self.message,
                "changes":     self._fix_changes(),
            }
        }
                        
    def as_json(self, pretty=False):
        """
        Return the activity structure as json
        """
        if pretty:
            return json.dumps(self.as_struct(), sort_keys=True, indent=2)
        return json.dumps(self.as_struct())

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        if LOG:
            LOG.error("post-receive failed: %s" % e)
        raise Exception("post-receive failed: %s" % e)
