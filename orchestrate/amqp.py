# coding=utf-8
# Copyright 2013 CollabNet, Inc.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pika

class AmqpConnectionError(Exception): pass

class AmqpServerConnection(object):
    """
    Object wrapping a connection to the amqp server, as defined in by 
    configuration.
    """

    def __init__(self, cfg, logger):
        """
        Constructor.
        """
        self.cfg = cfg
        self.logger = logger   
            
        self.credentials = pika.credentials.PlainCredentials(
            cfg.amqp_credentials['username'],
            cfg.amqp_credentials['password'])
        self.conn = None
        self.chan = None
        self.conn_params = cfg.amqp_connection_params
        self.queue_params = cfg.amqp_queue_params
        self.exchange_name = cfg.amqp_exchange_params['exchange']
        self.queue_name = cfg.amqp_queue_params['queue']
        self.exchange_params = cfg.amqp_exchange_params
        self.publish_params = cfg.amqp_publish_params

    def connect(self):
        """
        Create a new connection to the server, along with the necessary queue
        and exchange declarations.
        """
        self.logger.debug("Establishing connection ...")
        try:
            self.conn = pika.BlockingConnection(
                pika.ConnectionParameters(
                    credentials=self.credentials,
                    **self.conn_params))
        except Exception as e:
            msg = "Unable to connect to amqp server; check "\
                  "configuration (Reporter=%s; msg=%s)" % (e.__class__, e)
            raise AmqpConnectionError(msg)
        self.logger.debug("... connection established.")

        self.chan = self.conn.channel()
        self.chan.queue_declare(**self.queue_params)
        # Declare the exchange (UNLESS we are using the default, nameless
        # queue):
        if self.exchange_name:
            self.chan.exchange_declare(**self.exchange_params)

    def send(self, body):
        """
        Publish a message over the established connection.
        """
        self.logger.debug("Sending message...")

        try:
            self.chan.basic_publish(body=body,
                                    exchange=self.exchange_name,
                                    routing_key=self.queue_name,
                                    properties=pika.BasicProperties(
                                        **self.publish_params))
        except Exception as e:
            raise Exception("Unable to send to amqp server "\
                            "(Reported error=%s)" % e)
        self.logger.debug("... message sent.")

    def close(self):
        """
        Close connection to server.
        """
        self.conn.close()
