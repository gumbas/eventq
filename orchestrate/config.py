# coding=utf-8
# Copyright 2013 CollabNet, Inc.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import os

# Python 2/3 compatibility
try:
    import configparser
except ImportError:
    import ConfigParser as configparser

from orchestrate.configdefaults import CONFIG_DEFAULTS

###############################################################################

class NoConfigError(Exception): pass

class BadConfigError(Exception): pass

class ConfigValidationError(Exception): pass

class HookConfig(object):
    """
    Configuration for this hook script.  Hold a bunch of configuration hashes
    which can be overridden in a user-supplied configuration file.
    
    NOTE: any dictionary here can be overridden in configuration, but many are
    not documented in the sample configuration files, as modifying their values
    would result in hilarity.  And by hilarity, I mean soul-sucking sadness.
    """

    # Python's way of creating a singleton.  Prevents multiple calls to 
    # ConfigParser.  Void where prohibited by law.
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(HookConfig, cls).__new__(
                cls, *args, **kwargs)
        return cls._instance

    def __init__(self, cfg_file=None):
        # NOTE: cfg_file is assumed to be the absolute path to the file.
        if not (cfg_file and os.path.isfile(cfg_file)): 
            raise NoConfigError("Could not find cfg file %s" % cfg_file)

        self.cfg_file = cfg_file

        # Section of the cfg file that we're primarily interested in:
        amqp_cfg_section = 'RabbitMQ'

        # Read config file.
        self.cfg = configparser.ConfigParser()

        try:
            self.cfg.read(self.cfg_file)
        except Exception as e:
            raise NoConfigError("Unable to read cfg file %s"
                                % self.cfg_file)

        # Read AMQP settings (and defaults, if needed)
        self.amqp_cfg = ConfigWrapper(self.cfg_file)
        self.amqp_settings = self.amqp_cfg[amqp_cfg_section]

        # Default association key.  If we see this, things are not going well.
        self._default_association_key = 'PLEASE_SET_ASSOCIATION_KEY'

        # Overridable dicts:
        #
        # The following are sets of dict objects, which get passed to various
        # methods as wads of named paramters (i.e., dereferenced when passed to
        # method calls).  The purpose is to provide simplified, centralized
        # user-facing configuration for the common required settings (under the
        # RabbitMQ section), while allowing for the possibility of adding or
        # overriding parameters not explicity mentioned here.
        #
        # They get populated as follows:
        #
        # 1. From the defaults contained in configdefaults.py (above)
        # 2. From the 'RabbitMQ' section of the user-supplied config (above, as
        #    self.amqp_settings)
        # 3. Explicit assignment to individual dicts from self.amqp_settings
        #    (below, in declarations)
        # 4. Finally, any named sections in the user's config will have their
        #    key/value pairs added the corresponding named dict below. 
        #
        # For example, if the following was in the user's config file, it would
        # end up overriding the dict below, and passing the resulting dict to
        # the connection method:
        #
        # [amqp_connection_params]
        # connection_attempts = 2
        # backpressure_detection = True

        # Logging setup (gets passed to logging.basicConfig()):
        self.logging = {
            'filename': '/tmp/default-hooks.log',
            'level': 'info',
            'format': '%(asctime)s [%(levelname)s]: %(message)s'
        }

        self.svn = {
            'svnlook': '/usr/bin/svnlook'
        }

        self.git = {
            'find_copies_harder' : 'true',
            'rename_threshold'   : 40,
            'copy_threshold'     : 75
        }

        # Credentials (passed to pika.PlainCredentials()):
        self.amqp_credentials = {
            'username': self.amqp_settings['username'],
            'password': self.amqp_settings['password'],
        }

        # Connection settings (passed to pika.ConnectionParameters() along with
        # credentials):
        self.amqp_connection_params = {
            'host': self.amqp_settings['host'],
            'port': self.amqp_settings['port'],
            'virtual_host': self.amqp_settings['vhost'],
        }

        # Queue settings (passed to connection channel's
        # queue_declare() method): 
        self.amqp_queue_params = {
            'queue': self.amqp_settings['queue_name'],
            'durable': 'true'
        }

        # Exchange settings (passed to connection's channel's
        # exchange_declare() method):
        self.amqp_exchange_params = {
            'exchange': self.amqp_settings['exchange'],
            'type': 'fanout'
        }

        # Publish settings (passed to connection's channel's
        # basic_publish() method):
        self.amqp_publish_params = {
            'content_type': 'text/plain',
            'delivery_mode': 2
        }

        self.orchestrate = {
            'source_association_key': self._default_association_key
        }

        # Process remaining sections in the config file, if present, and 
        # modify/assign to the above config dicts:
        for sect in self.cfg.sections():
            # Skip processing of the primary config section - we already read it
            # in above.
            if sect == amqp_cfg_section: continue
            for optn in self.cfg.options(sect):
                if not hasattr(self, sect):
                    self.__dict__[sect] = {}
                self.__dict__[sect][optn] = self.cfg.get(sect, optn)

        # Fix the types of some properties:
        try:
            self.logging['level'] =\
              getattr(logging, self.logging['level'].upper())
            self.amqp_connection_params['port'] =\
              int(self.amqp_connection_params['port'])
            self.amqp_publish_params['delivery_mode'] =\
              int(self.amqp_publish_params['delivery_mode'])
            if self.amqp_queue_params['durable'].lower() == 'true':
                self.amqp_queue_params['durable'] = True
            else:
                self.amqp_queue_params['durable'] = False
            if self.git['find_copies_harder'].lower() == 'true':
                self.git['find_copies_harder'] = True
            else:
                self.git['find_copies_harder'] = False
        except Exception as e:
            raise BadConfigError("Type massaging failed: %s" % e)

    def validate(self):
        """
        Sanity check.  Raises exception if error found.
        """
        if not (self.orchestrate.has_key('source_association_key') and
           self.orchestrate['source_association_key']):
            raise ConfigValidationError("Config validation failed: "\
                                        "source_association_key not set.")
        if self.orchestrate['source_association_key'] ==\
           self._default_association_key:
            raise ConfigValidationError("Config validation failed: "\
                                        "source_association_key not set.")

#
# Wraps ConfigParser with some functionality that's a bit more sane (exception-wise)
#
class ConfigWrapperError(Exception): pass

class InvalidConfigFile(ConfigWrapperError): pass

class NoneDict(dict):
    def __getitem__(self, key):
        return dict.get(self, key)

class ConfigWrapper(object):
    """
    Wraps ConfigParser with a slightly less exception-driven interface.
    """
    config_file = None

    def __init__(self, config_file, defaults=CONFIG_DEFAULTS):
        """
        Constructor
        """
        self.config_file = config_file
        self.defaults = NoneDict(defaults)

        self.config = configparser.ConfigParser()
        self.config.read(self.config_file)
    #---

    def __getitem__(self, section_name):
        """
        Implements a dict-style lookup for config.  Ex: config['orchestrate']['api_version']

        :param section_name: Name of the config section that we're trying to access
        :type section_name: str
        :return: dict
        """
        response = NoneDict({})
        if self.defaults.has_key(section_name):
            response = NoneDict(self.defaults[section_name])

        try:
            items = self.config.items(section_name)
            response.update(dict(items))
        except configparser.NoSectionError:
            pass

        return response
    #---

    def get(self, section, item, default_value = None):
        """
        Re-implements 'get' only without exceptions.

        :param section: Section to fetch from
        :type section: str
        :param item: Item whose value we want
        :type item: str
        :return: str
        """
        try:
            return self.config.get(section, item)
        except configparser.NoSectionError:
            return default_value
        except configparser.NoOptionError:
            return default_value
    #---
#---
