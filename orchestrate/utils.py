# coding=utf-8
# Copyright 2013 CollabNet, Inc.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import datetime
import logging
import re

#
# Creates a logger on demand.
#
def create_logger(cfg=None):
    if not cfg:
        raise Exception("No configuration supplied to logger")
    logging.basicConfig(**cfg.logging)
    return logging.getLogger(__name__)

def fix_date(timestamp):
    # Convert svn's '2012-11-27 11:55:21 -0800 (Tue, 27 Nov 2012)' or
    # Convert git's '2012-11-27 11:55:21 -0800' to 
    # to RFC3339's  '2012-11-27T19:55:21Z'

    # Remove svn's human-friendly formtatting:
    timestamp = re.sub('\s+\([^)]+\)', '', timestamp)

    # Make sure we match the expected pattern before we start trying to 
    # massage the date.  If it doesn't match, we'll just return the 
    # timestamp string we got in the first place:
    match = re.match('(\d+-\d+-\d+\s+\d+:\d+:\d+)\s+([-+])(\d\d)(\d\d)',
                     timestamp)
    if match:
        timestamp = match.group(1)
        direction = match.group(2)
        delta_h = match.group(3)
        delta_m = match.group(4)

        dt = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
        delta = datetime.timedelta(hours=int(delta_h),
                                   minutes=int(delta_m))
        if direction == '-':
            dt = dt + delta
        else:
            dt = dt - delta
        timestamp = dt.strftime("%Y-%m-%dT%H:%M:%SZ")
    return timestamp
