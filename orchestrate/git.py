# Copyright 2013 CollabNet, Inc.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import subprocess

class CommandRunner(object):
    
    def __init__(self):
        pass

    def get_output(self, cmd_str):
        """    
        Return the output of the command being run as an array of lines (trailing
        whitespace removed).  
    
        Assumes that the command string is splittable on whitespace.
        """
        proc = subprocess.Popen(cmd_str.split(), stdout=subprocess.PIPE)
        proc.wait()
        return [a.rstrip() for a in proc.stdout.readlines()]


class GitTool(object):
    """
    Wrapper class for common git commands.
    
    NOTE: Methods on this class *must* be invoked while within a working copy;
    doing so outside a working copy will produce errors.
    """
    def __init__(self, cmd_runner=CommandRunner()):
        self.cmd_runner = cmd_runner
        
    def get_commit_data(self, commit_ref, find_copies_harder=True, 
                        rename_threshold=40, copy_threshold=75, **kwargs):
        """
        Given a commit reference, return a data object containing information
        about the commit, gleaned from a variety of git commands.
        """
        message = ''
        additional_data = {}
        timestamp = user = None
        changed = []
       
        find_harder_str = '--find-copies-harder' if find_copies_harder else ''
       
        cmd = 'git cat-file commit %s' % commit_ref
        # Output of command is expected to be a series of key/value pairs, one
        # per line, followed by a blank line, followed by the commit message.

        output = self.cmd_runner.get_output(cmd)

        # The first N lines before the commit message are a series of lines
        # that contain tree, parent, author, and committer.  We don't currently
        # do anything with this data, but stash them in a hash, for posterity:
        additional_data = dict([p.strip().split(' ',1) 
                                for p in output[0: output.index('')]])

        # Commit message starts after the first blank line:
        message = '\n'.join(output[output.index('') + 1:])

        # TODO: This has the potential to be both expensive, and unreliable;
        # expensive by documentation, and unreliable in that copies and 
        # renames are not tracked, but inferred by walking the tree and looking
        # for candidates.
        #
        # --find-copies and --find-renames might be preferrable, or even 
        # dropping this flag altogether, and simply not reporting copies and
        # renames separately from add/modify/delete operations.

        cmd = 'git log -1 --name-status %s -M%s%% -C%s%% --date=iso '\
              ' --pretty=format:%%ad|%%aE ' % (find_harder_str, 
                                               rename_threshold, 
                                               copy_threshold) + commit_ref

        # Output of command is expected to be a line containing the timestamp
        # and email of user (pipe separated), followed by a series of lines 
        # containing the change type code, and one or more path (depending on 
        # the type of change).
        
        output = self.cmd_runner.get_output(cmd)       
        timestamp, user = output[0].split('|')
        changed = [a.split() for a in output[1:]]

        return data(message=message,
                    additional_data=additional_data, 
                    user=user,
                    timestamp=timestamp,
                    changed=changed)

    def get_commits(self, old, new):
        """
        Given a pair of revisions, returns a list of the commits between them.
        """
        cmd = 'git rev-list %s..%s' % (old, new)
        return self.cmd_runner.get_output(cmd)

    def get_branch_name(self, refname):
        """
        Given a git branch reference, return the symbolic name for that branch.
        """
        cmd = 'git rev-parse --symbolic --abbrev-ref %s' % refname
        return self.cmd_runner.get_output(cmd)[0]

class data(object):
    """
    Bare object for writing arbitrary data to.
    """
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        
